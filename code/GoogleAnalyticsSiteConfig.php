<?php

class GoogleAnalyticsSiteConfig extends DataExtension {

    private static $db = array(
        'GoogleAnalyticsCode' => 'Text'
    );

    public function updateCMSFields(FieldList $fields) {
        HtmlEditorConfig::set_active('textonly');
        $fields->addFieldsToTab("Root.Main", HeaderField::create('GoogleHeader1', 'Google Analytics Code', 3));
        $fields->addFieldsToTab('Root.Main', LiteralField::create('GoogleLiteral1', '<p class="message error" style="margin-top:20px;">Only change this if you <strong>REALLY</strong> know what your are doing.</p>'));
        $fields->addFieldsToTab('Root.Main', TextareaField::create('GoogleAnalyticsCode', 'Google Analytics Code'));
    }

}